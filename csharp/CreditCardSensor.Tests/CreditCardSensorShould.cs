using System;
using Xunit;

namespace CreditCardSensor.Tests
{
    public class CreditCardSensorShould
    {
        private readonly CreditCardManager _sut;

        public CreditCardSensorShould()
        {
            _sut = new CreditCardManager();
        }

        [Fact]
        public void AcceptNumericsAndSpacesOnly()
        {
            Assert.Null(_sut.CensorCardNumber("2A44 9891 EEEE FFFF"));
            Assert.Null(_sut.CensorCardNumber("2A4@ 9oa91 E!*E xADF"));
            Assert.Null(_sut.CensorCardNumber("1515 A1051 0510 2051"));
            Assert.Null(_sut.CensorCardNumber("501861-1-591951"));
            
            Assert.NotNull(_sut.CensorCardNumber("5015 2591 0512 1516"));
            Assert.NotNull(_sut.CensorCardNumber("5015  25910512 1516"));
        }

        [Fact]
        public void AcceptInputsWithinLength()
        {
            // Less than 13 digits
            Assert.Null(_sut.CensorCardNumber("123456780 123", 13));

            // More than 19 digits
            Assert.Null(_sut.CensorCardNumber("1234 5678 9012 3456 7890", 19));

            // Within 13-19 digits
            Assert.NotNull(_sut.CensorCardNumber("123 56780 52155", 13));
            Assert.NotNull(_sut.CensorCardNumber("123 5678 9012 3456 7890", 19));
        }

        [Theory]
        [InlineData("1244 5005 3701 9993", "xxxx xxxx xxxx 9993")]
        [InlineData("67619600 0000551045", "xxxxxxxx xxxxxx1045")]
        public void CensorLastFourDigits(string cardNumber, string censoredCardNumber){
            Assert.Equal(_sut.CensorCardNumber(cardNumber),censoredCardNumber);
        }

        [Theory]
        [InlineData("1244 5005 3701 9993", 13, 19, "x", 4, "xxxx xxxx xxxx 9993")]
        [InlineData("67619600 0000551045", 13, 19, "x", 4, "xxxxxxxx xxxxxx1045")]
        public void CensorCardNumberDigits(string cardNumber, int minCardDigit, int maxCardDigit, string replacementText, int showLastDigits, string censoredCardNumber){
            Assert.Equal(_sut.CensorCardNumber(cardNumber, minCardDigit, maxCardDigit, replacementText, showLastDigits), censoredCardNumber);
        }
    }
}
