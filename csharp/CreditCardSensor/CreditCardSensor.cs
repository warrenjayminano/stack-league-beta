using System;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;

public class CreditCardManager
{
    public string CensorCardNumber(string cardNumber, int minCardDigit = 13, int maxCardDigit = 19, string replacementText = "x", int showLastDigits = 4)
    {
        if(string.IsNullOrEmpty(cardNumber)) {
            return null;
        }

        if(!isCardNumberValid(cardNumber, minCardDigit, maxCardDigit)){
            return null;
        }

        return getCensoredCardNumber(cardNumber, replacementText, showLastDigits);
    }

    // Validate card number
    bool isCardNumberValid(string cardNumber, int minLength, int maxLength){
        
        return (isLengthValid(cardNumber, minLength, maxLength) && 
                isNumericSpacesOnly(cardNumber));
    }

    // Validate number of digits on card number
    bool isLengthValid(string cardNumber, int minLength, int maxLength){
        int digitsCount = cardNumber.Count(c => Char.IsDigit(c));
        return digitsCount >= minLength && digitsCount <= maxLength;
    }

    // Validate number and spaces only
    bool isNumericSpacesOnly(string cardNumber){
        return Regex.IsMatch(cardNumber, @"^[\d\s]+$");
    }

    // Return card number with censored section and plain text last digits
    string getCensoredCardNumber(string cardNumber, string replacementText, int showLastDigits){
        StringBuilder sb = new StringBuilder();

        int censorEndIndex = getCensorEndIndex(cardNumber, showLastDigits);
        sb.AppendFormat("{0}{1}",
        censorNumerics(cardNumber.Substring(0, censorEndIndex + 1), replacementText),
        cardNumber.Substring(censorEndIndex + 1)
        );

        return sb.ToString();
    }

    // Get last index of card number to censor
    int getCensorEndIndex(string cardNumber, int showLastDigits){
        int lastDigitCount = 0;
        int censorEndIndex = 0;
        for(int i = (cardNumber.Length - 1); i >= 0; i--){
            censorEndIndex = i;
            if(Char.IsDigit(cardNumber[i])){
                lastDigitCount++;
                if(lastDigitCount == showLastDigits){
                    break;
                }
            }
        }
        return censorEndIndex - 1;
    }

    // Mask card number section with specified replacementText
    string censorNumerics(string censorText, string replacementText){
        if(censorText == null) {
            throw new ArgumentNullException(nameof(censorText));
        }

        return censorText.Length > 0 ? Regex.Replace(censorText, "[0-9]", replacementText) : string.Empty;
    }
}
