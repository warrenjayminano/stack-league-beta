﻿using System;

namespace CreditCardSensor
{
    class Program
    {
        static void Main(string[] args)
        {
            var ccm = new CreditCardManager();
            Console.WriteLine("Input Card Number:");
            string input = Console.ReadLine();
            string output = ccm.CensorCardNumber(input);
            Console.WriteLine($"Card Number: {output}. Press any key to exit...");
            Console.ReadKey();
        }
    }
}
